type CharacterProps = {
  id: number;
  status: string;
  type: string;
  gender: string;
  species: string;
  image: string;
  name: string;
};
