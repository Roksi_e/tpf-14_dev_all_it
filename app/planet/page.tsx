"use client";

import PlanetCard from "@/components/ui/planetCard";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

async function getDataPlanet(): Promise<{
  info: Info;
  results: ILocation[];
}> {
  try {
    const res = await axios.get("https://rickandmortyapi.com/api/location");
    return res.data;
  } catch (error) {
    throw new Error("Failed to fetch data");
  }
}

export default function PlanetsPage() {
  const { data: planets } = useQuery({
    queryKey: ["planets"],
    queryFn: getDataPlanet,
  });

  return (
    <div className="grid grid-cols-5  relative w-[80%] h-3/5 mx-auto">
      {planets?.results.map((planet) => (
        <PlanetCard
          key={planet.id}
          id={planet.id}
          name={planet.name}
          type={planet.type}
          dimension={planet.dimension}
          residents={planet.residents}
          url={planet.url}
          created={planet.created}
        ></PlanetCard>
      ))}
    </div>
  );
}
