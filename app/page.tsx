import Image from "next/image";
import fondo from "@/components/images/fondo.png";
import navLogo from "@/components/images/navLogo.png";
import ufo from "@/components/images/ufo.png";
import Link from "next/link";

export default function Home() {
  return (
    <div className="w-full h-[90vh] flex flex-col justify-around gap-8">
      <div className=" w-[100%] h-32 relative flex items-center  flex-col  z-10 ">
        <Image src={navLogo} alt="logo" />
      </div>
      <div className="z-10 w-[100%] h-[60vh] relative flex justify-center my-auto py-14">
        <Link href="/planet">
          <Image src={ufo} alt="ufo" />
        </Link>
      </div>
      <Image src={fondo} alt="fondo" fill style={{ objectFit: "cover" }} />
    </div>
  );
}
