import Header from "@/components/ui/header";
import Providers from "@/lib/providers";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Characters",
};

export default function CharacterLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="min-h-[80vh] flex flex-col ">
      <Providers>
        <Header />
        <main className="p-2 w-full min-h-[80vh]">{children}</main>
      </Providers>
    </div>
  );
}
