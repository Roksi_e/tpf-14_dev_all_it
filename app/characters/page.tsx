"use client";

import CharacterCard from "@/components/ui/characterCard";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useAddFavorite } from "@/data/store";

async function getData(): Promise<{
  info: Info;
  results: Character[];
}> {
  try {
    const res = await axios.get(`https://rickandmortyapi.com/api/character`);
    return res.data;
  } catch (error) {
    throw new Error("Failed to fetch data");
  }
}

export default function CharactersPage() {
  const { data: characters } = useQuery({
    queryKey: ["characters"],
    queryFn: getData,
  });

  const { addToFav } = useAddFavorite();

  return (
    <div className="mx-auto w-[60vw] flex flex-row flex-wrap justify-center gap-8 ">
      {characters?.results.map((char) => (
        <div
          key={char.id}
          className="relative bg-positive rounded p-4 w-64 h-80"
          onClick={(e) => {
            addToFav(char);
          }}
        >
          <CharacterCard
            key={char.id}
            id={char.id}
            status={char.status}
            type={char.type}
            gender={char.gender}
            species={char.species}
            image={char.image}
            name={char.name}
            origin={char.origin}
            location={char.location}
            episode={char.episode}
            url={char.url}
            created={char.created}
          ></CharacterCard>
        </div>
      ))}
    </div>
  );
}
