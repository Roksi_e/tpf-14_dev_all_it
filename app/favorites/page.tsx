/* eslint-disable react/no-unescaped-entities */
"use client";

import CharacterCard from "@/components/ui/characterCard";
import Image from "next/image";
import favorites from "@/components/images/favorites 1.png";
import { Input } from "@/components/ui/input";
import { useAddFavorite } from "@/data/store";

export default function FavoritesPage() {
  const { characters, deleteToFav } = useAddFavorite();

  return (
    <div className="relative w-full min-h-[80vh] flex flex-col justify-between p-8">
      <div className="w-[60vw] h-[175px] mx-auto relative">
        <Image
          src={favorites}
          alt="favorites"
          fill
          style={{ objectFit: "contain" }}
        />
      </div>
      <div className="w-[400px] h-[70px] mx-auto">
        <Input className="bg bg-positive w-[100%] h-10" />
      </div>
      <div className="mx-auto w-[60vw] flex flex-row flex-wrap justify-center min-h-80 gap-8  ">
        {characters.length !== 0 ? (
          characters.map((char) => {
            return (
              <>
                <div
                  key={char.id}
                  onClick={() => {
                    deleteToFav(char.id);
                  }}
                  className="relative bg-positive rounded p-4 w-64 h-80"
                >
                  <CharacterCard
                    key={char.id}
                    id={char.id}
                    status={char.status}
                    type={char.type}
                    gender={char.gender}
                    species={char.species}
                    image={char.image}
                    name={char.name}
                    origin={char.origin}
                    location={char.location}
                    episode={char.episode}
                    url={char.url}
                    created={char.created}
                  ></CharacterCard>
                </div>
              </>
            );
          })
        ) : (
          <div className=" text-white h-80 text-4xl ">
            There doesn't seem to be anything to show here currently, if you see
            a character you want to add to favorites, hit the white star so you
            can see them here, change their name and assign them a rating!
          </div>
        )}
      </div>
    </div>
  );
}
