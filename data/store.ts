import create from "zustand";
import { persist, createJSONStorage } from "zustand/middleware";

interface AddToFavorite {
  characters: Character[];
  addToFav: (newCharacter: Character) => void;
  deleteToFav: (id: number) => void;
}

export const useAddFavorite = create<AddToFavorite>(
  persist(
    (set, get) => ({
      characters: [],

      addToFav: (newCharacter) => {
        const { characters } = get();
        set(() => ({
          characters: [newCharacter, ...characters],
        }));
      },

      deleteToFav: (id: number) => {
        const { characters } = get();
        set({
          characters: characters.filter((char) => char.id !== id),
        });
      },
    }),

    {
      name: "favorites",
      storage: createJSONStorage(() => localStorage),
    }
  )
);
