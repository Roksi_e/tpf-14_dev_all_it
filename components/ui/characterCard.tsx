import Link from "next/link";
import Image from "next/image";
import { Button } from "./button";
import { Checkbox } from "./checkbox";

export default function CharacterCard({ id, name, status, image }: Character) {
  return (
    <>
      <Checkbox
        id={`${id}`}
        className="bg-primary absolute z-10 top-0 left-0 w-[30px] h-[30px] border-spacing-2 rounded-full"
      ></Checkbox>
      <div className="relative h-[180px] w-full">
        <Image src={image} alt={name} fill sizes="300px" className="cover" />
      </div>
      <div className="flex flex-col">
        <strong>Name</strong>
        <small>{name}</small>
        <strong>Status</strong>
        <small>{status}</small>
      </div>
      <Button
        className="bg-black absolute top-[60%] right-[20px] shadow-[0_0_0_3px_rgba(202,250,130,1)]"
        shape="round"
        size="large"
      >
        <Link href={`/characters/${id}`}>GO</Link>
      </Button>
    </>
  );
}
